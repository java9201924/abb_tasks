Task1:
Create a Car class with private properties:
 make (String),
 model (String),
 year (int),
 rentalPrice (double).
Use encapsulation principle for this properties. Ensure that the properties cannot be accessed directly from outside the class

Task2:
Create an abstract class called Employee with properties name and salary. Implement the following methods:
getDetails(): Prints the employee's name and salary.
Create the following subclasses that inherit from Employee:
Manager: Representing a manager with an additional property department. Implement the getDetails() method to include the department information.
Developer: Representing a developer with an additional property programmingLanguage. Implement the getDetails() method to include the programming language information.

Task3:
Create an interface called Playable with methods play() and stop(). Then create the following classes that implement Playable:
AudioPlayer: Representing an audio player. Implement the play() and stop() methods to control audio playback.
VideoPlayer: Representing a video player. Implement the play() and stop() methods to control video playback.

Task4:
Create an abstract class called Car with the following properties:
make (String)
model (String)
year (int)
rentalRate (double)
Implement getter and setter methods for the properties in the Car class.
Declare an abstract method called calculateRentalCharge() in the Car class that calculates and returns the rental charge based on the number of days.
Implement a concrete method called displayCarInfo() in the Car class that displays the car information, including the make, model, year, and rental rate.

Create two subclasses: EconomyCar and LuxuryCar, which inherit from the Car class.
The EconomyCar class should have an additional property called fuelEfficiency (double). Implement appropriate getter and setter methods for this property.
The LuxuryCar class should have an additional property called premiumFeatures (String[]), which represents a list of premium features available in the car. Implement appropriate getter and setter methods for this property.

Create an interface called Rentable with the following methods:
rent(int numDays): Accepts the number of days for renting the car and updates its availability status.
returnCar(): Updates the availability status of the car upon return.
Implement the Rentable interface in the Car class and its subclasses to provide the rental functionality.

Create a class called RentalTransaction with the following properties:
car (Car)
customerName (String)
rentalDays (int)
Implement getter and setter methods for the properties in the RentalTransaction class.
Implement a method called calculateRentalCost() in the RentalTransaction class that calculates and returns the total rental cost based on the car's rental rate and the number of rental days.
Implement a method called displayTransactionInfo() in the RentalTransaction class that displays the rental transaction information, including the car details, customer name, rental days, and rental cost.

