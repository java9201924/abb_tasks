package src;

public class CarRunner {
    public static void main(String[] args) {
        EconomyCar economyCar = new EconomyCar("Toyota", "Corolla", 2022, 50.0, 25.0);
        economyCar.setAvailable(true);

        String[] premiumFeatures = {"Leather seats", "Navigation system", "Sunroof"};
        LuxuryCar luxuryCar = new LuxuryCar("Mercedes-Benz", "S-Class",2023, 150.0, premiumFeatures);
        luxuryCar.setAvailable(true);

        RentalTransaction transaction1 = new RentalTransaction(economyCar, "John Smith", 7);
        RentalTransaction transaction2 = new RentalTransaction(luxuryCar, "Emily Johnson", 5);

        transaction1.displayTransactionInfo();
        System.out.println();
        transaction2.displayTransactionInfo();

        economyCar.rent(7);
        economyCar.returnCar();
    }

}
