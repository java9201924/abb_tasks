package src;

public class Developer extends Employee{
    private String programmingLanguage;
    public Developer(String name, double salary, String programmingLanguage) {
        super(name, salary);
        this.programmingLanguage = programmingLanguage;
    }
    public void setProgrammingLanguage(String programmingLanguage) {
        this.programmingLanguage = programmingLanguage;
    }
    public String getProgrammingLanguage() {
        return programmingLanguage;
    }
    @Override
    public void getDetails() {
        System.out.println("Developer's details");
        System.out.println("=================================");
        System.out.println("Name: " + getName());
        System.out.println("Salary: " + getSalary() + "$");
        System.out.println("Programming language: " + getProgrammingLanguage());
        System.out.println("==================================");
    }
}
