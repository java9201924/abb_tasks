package src;

public class EconomyCar extends Car {
    private double fuelEfficiency;
    public EconomyCar(String make, String model, int year, double rentalRate, double fuelEfficiency) {
        super(make, model, year, rentalRate);
        this.fuelEfficiency = fuelEfficiency;
    }
    public void setFuelEfficiency(double fuelEfficiency) {
        this.fuelEfficiency = fuelEfficiency;
    }
    public double getFuelEfficiency() {
        return fuelEfficiency;
    }

    @Override
    public double calculateRentalCharge(int numDays) {
        return getRentalRate() * numDays;
    }
}
