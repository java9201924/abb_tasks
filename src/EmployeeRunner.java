package src;

public class EmployeeRunner {
    public static void main(String[] args) {
        Manager manager = new Manager("John Doe", 26750,"Sales");
        manager.getDetails();
        System.out.println();
        Developer developer = new Developer("Anna Nicole", 5482, "Java");
        developer.setName("Alex Turner");
        developer.setSalary(6580);
        developer.setProgrammingLanguage("Python");
        developer.getDetails();
    }
}
