package src;

public interface Playable {
    void play();
    void stop();

}
