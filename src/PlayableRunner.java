package src;

public class PlayableRunner {
    public static void main(String[] args) {
        AudioPlayer audio = new AudioPlayer();
        audio.play();
        audio.stop();
        VideoPlayer video = new VideoPlayer();
        video.play();
        video.stop();
    }
}
