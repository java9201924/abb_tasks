package src;

public interface Rentable {
    void rent(int numDays);
    void returnCar();
}
