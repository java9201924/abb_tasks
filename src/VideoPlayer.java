package src;

public class VideoPlayer implements Playable{
    @Override
    public void play() {
        System.out.println("VideoPlayer is played...");
    }
    @Override
    public void stop() {
        System.out.println("VideoPlayer is stopped...");
    }
}
