package src.tasks;

public abstract class Animal {
    public abstract void makeSound();
}
