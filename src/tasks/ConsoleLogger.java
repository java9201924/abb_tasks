package src.tasks;

public class ConsoleLogger implements Logger{
    @Override
    public void logInfo(String message) {
        System.out.println("INFO: " + message);
    }

    @Override
    public void logWarning(String message) {
        System.out.println("WARNING: " + message);
    }

    @Override
    public void logError(String message) {
        System.out.println("ERROR: " + message);
    }
}
