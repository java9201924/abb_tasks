package src.tasks;

public class LoggerRunner {
    public static void main(String[] args) {

        ConsoleLogger consoleLogger = new ConsoleLogger();

        System.out.println("----------------------------------------------------------------------------------------------------");

        consoleLogger.logInfo("The application has started successfully!");
        consoleLogger.logWarning("Deprecated function 'getData()' is being used. Please update to 'fetchData()'.");
        consoleLogger.logError("NullPointerException: Unable to access object reference. Check for uninitialized variables.");

        System.out.println("----------------------------------------------------------------------------------------------------");

        FileLogger fileLogger = new FileLogger("test.txt");

        fileLogger.logInfo("The application has started successfully!\n");
        fileLogger.logWarning("Deprecated function 'getData()' is being used. Please update to 'fetchData()'.\n");
        fileLogger.logError("NullPointerException: Unable to access object reference. Check for uninitialized variables.");

        System.out.println("----------------------------------------------------------------------------------------------------");


    }
}
