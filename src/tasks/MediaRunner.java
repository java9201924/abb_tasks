package src.tasks;

public class MediaRunner {
    public static void main(String[] args) {

        Music music = new Music();
        music.play();
        Movie movie = new Movie();
        movie.play();
    }
}
