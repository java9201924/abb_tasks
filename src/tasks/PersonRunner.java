package src.tasks;

public class PersonRunner {
    public static void main(String[] args) {

        System.out.println();
        Student student = new Student("Lamia", 20, "14984567");
        student.displayInfo();
        System.out.println();
        Teacher teacher = new Teacher("Nuray", 32,"Computer Graphics");
        teacher.displayInfo();

    }

}
